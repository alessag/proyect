<!DOCTYPE html>
 <html class="no-js"> 
    <!-- Header -->
        <?php
            require('views/admin-header.php');
        ?>
    <!-- Header -->
    
    <body>
    <!-- Admin Side Bar -->
        <?php
            require('views/side-bar.php');
        ?>
    <!-- Admin Side Bar -->

    <!-- Right Panel -->
        <div id="right-panel" class="right-panel">

    <!-- Header-->
       <?php
            require('views/module-header.php');
        ?>
    <!-- Header-->

    <div class="content mt-3">
            <div class="content">
            <h1 class="pb-3">Servicio de Telefono</h1>
                <form method="POST" action="includes/telephone.php">
                    <div class="form-group row">
                        <label for="plan_name" class="col-sm-2 col-form-label">Nombre del plan</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="plan_name" class="form-control" id="plan_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="mb" class="col-sm-2 col-form-label">MB</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="mb" class="form-control" id="mb">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-2 col-form-label">SMS</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="sms" class="form-control" id="sms">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-2 col-form-label">MIN</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="min" class="form-control" id="min">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-2 col-form-label">Precio</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="price" class="form-control" id="price">
                        </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary center">Crear</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Right Panel -->

    <!-- Scripts -->
        <?php
            require('views/admin-footer.php');
        ?>
    <!-- Scripts -->

</html>