<?php
    session_start();
?>
<!doctype html>

<html class="no-js" lang="">
    <!-- Admin header -->
        <?php
            require('views/admin-header.php');
        ?>
    <!-- Admin header -->
<body>
    <!-- Admin Side Bar -->
        <?php
            require('views/side-bar.php');
        ?>
    <!-- Admin Side Bar -->
    
    <div id="right-panel" class="right-panel">

        <!-- Header-->
            <?php
                require('views/module-header.php');
            ?>
        <!-- Header-->

        <div class="content mt-3">
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Success</span> You successfully read this important alert message.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
        <?php
            require('views/admin-footer.php');
        ?>
    <!-- Scripts -->

</body>
</html>
