<?php

function error($error){
    header("Location: ../signup.php?signup=".$error);
    exit(); // Close the script
}

if(isset($_POST['submit'])){

    include_once 'dbh.inc.php';
    
    $first = mysqli_real_escape_string($conn, $_POST['first']);
    $last = mysqli_real_escape_string($conn, $_POST['last']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);

    //Error handlers 
    //Chech for empty fields
    if(empty($first) || empty($last) || empty($email) || empty($uid) || empty($pwd)){
        error('empty');
    }
    else{
        //Okeeeey, let me se, here we check if input characters are valid
        if(!preg_match('/^[a-zA-Z0-9 ]+$/', $first) || !preg_match('/^[a-zA-Z0-9 ]+$/', $last)){
            error('invalid');
        }else{
            //check if the email is vaqlid 
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
               error('email');
            }else{
                //Crreate a connection to the db 
                $sql = "SELECT * FROM users WHERE user_uid='$uid'";
                $result = mysqli_query($conn, $sql);
                $resultCheck = mysqli_num_rows($result);

                if($resultCheck > 0){
                    error('usertaken');
                } else{
                    // Hashing the password 
                    $hashPwd = password_hash($pwd, PASSWORD_DEFAULT);
                    //Inset the user to the datanase
                    $sql = "INSERT INTO users (user_first, user_last, user_email, user_uid, user_pwd) VALUES ('$first', '$last', '$email', '$uid', '$hashPwd');";
                    mysqli_query($conn, $sql);
                    error('success');
                }
            }
        }
    }

}
else{
    header("Location: ../login.php");
    exit(); // Close the script
}