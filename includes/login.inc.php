<?php

session_start();

if(isset($_POST['submit'])){

    include 'dbh.inc.php';

    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);

    //Error handlers - Manejo de errores 
    //we check if inputs are empty
    if(empty($uid) || empty($pwd)){
        header("Location: ../index.php?login=empty");
        exit();
    } else{
        //Vamos a ver si usernma existe 
        $sql = "SELECT * FROM users WHERE user_uid='$uid' OR user_email='$uid'";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);
        if($resultCheck < 1){
            header("Location: ../index.php?login=error");
            exit();
        }else{
            if($row = mysqli_fetch_assoc($result)){
                //echo $row['user_uid'];
                // de-hashing the password 
                $hashedPwdCheck = password_verify($pwd, $row['user_pwd']);
                if($hashedPwdCheck == false){
                    header("Location: ../index.php?login=error");
                    exit();
                } elseif ($hashedPwdCheck ==true){
                    // log in the user here
                    //super global we have acces to all 
                    $_SESSION['u_id'] = $row['user_id'];
                    $_SESSION['u_first'] = $row['user_first'];
                    $_SESSION['u_last'] = $row['user_last'];
                    $_SESSION['u_email'] = $row['user_email'];
                    $_SESSION['u_uid'] = $row['user_uid'];
                    
                    //Hasta aqui ya sabemos que el usuario existe en la base de datos y la contraseña es correcta y todo eso
                    // Ahora vamos a ver si es de tipo admin o user

                    $sql = "SELECT * FROM users WHERE (state=1 AND user_uid='$uid')";
                    $result = mysqli_query($conn, $sql);

                    if($row = mysqli_fetch_assoc($result)){
                        session_start();
					    $_SESSION['admin'] = $uid;
                        header("Location: ../module.php");
                    }
                    else{
                        header("Location: ../index.php?login=succes");
                    }
                    // if($result) header("Location: ../module.php");
                    // else header("Location: ../index.php?login=succes");
                    // exit();

                }
            }
        }
    }
} else{
    header("Location: ../index.php?login=error");
    exit();
}