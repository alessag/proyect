<div id="navbar-container">
    <nav class="container navbar navbar-expand-lg">
        <a class="navbar-brand" href="index.php"><img src="images/desktop.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                <a class="nav-link" href="#">Contáctenos <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <?php
                    if(isset($_SESSION['u_id'])){
                    echo ' <form action="includes/logout.inc.php" method="POST">
                    <button type="submit" name="submit" class="btn btn-button">Logout</button>
                    </form>';
                    } else{
                    echo ' <form action="includes/login.inc.php" method="POST">
                    <a class="nav-link" href="login.php">Ingresar</a>
                    </form>';
                    }
                    ?>
                </li>
            </ul>
        </div>
    </nav>
</div>