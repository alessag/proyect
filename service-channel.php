<!DOCTYPE html>
 <html class="no-js">
    <!-- Header -->
        <?php
            require('views/admin-header.php');
        ?>
    <!-- Header -->

    <body>
    <!-- Admin Side Bar -->
        <?php
            require('views/side-bar.php');
        ?>
    <!-- Admin Side Bar -->

    <!-- Right Panel -->
        <div id="right-panel" class="right-panel">

     <!-- Header-->
        <?php
            require('views/module-header.php');
        ?>
    <!-- Header-->

        <div class="content mt-3">
                <div class="content">
                <h1 class="pb-3">Canales</h1>
                        <div class="form-group row">
                          <form name="add_channel" id="add_channel">
                            <div class="table-responsive">
                               <table class="table table-bordered" id="dynamic_field">
                                    <tr>
                                         <td><input type="text" name="channel[]" placeholder="Nombre del canal" class="form-control name_list" /></td>
                                         <td><button type="button" name="add" id="add" class="btn btn-success">Añadir otro canal</button></td>
                                    </tr>
                               </table>
                               <input type="button" name="submit" id="submit" class="btn btn-info" value="submit" />
                             </div>
                          </form>
                            <!--<label  class="col-sm-2 col-form-label">Nombre del canal</label>
                            <div class="col-sm-10 col-md-2">
                            <input type="text" name="name" class="form-control" id="plan_name">-->
                          </div>
                        </div>
                </div>
            </div>
        </div>
    <!-- Right Panel -->


    <!-- Scripts -->
        <?php
            require('views/admin-footer.php');
        ?>
    <!-- Scripts -->

</html>


<script>
  $(document).ready(function(){
    var i=1;
    $('#add').click(function(){
      i++;
      $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="channel[]" placeholder="Nombre del canal" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
    });
    $(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr("id");
      $('#row'+button_id+'').remove();
    });
    $('#submit').click(function(){
      $.ajax({
       url:"includes/channel.php",
       method:"POST",
       data:$('#add_channel').serialize(),
       success:function(data)
       {
            alert(data);
            $('#add_channel')[0].reset();
       }
      });
    });
  });
</script>
