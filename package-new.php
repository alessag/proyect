<?php
/*
  include_once 'includes/dbh.inc.php';
  $sql="SELECT * FROM cable";
  $result = mysqli_query($conn,$sql) or die( "Error en query:".mysql_error() );
  $option="<option value='0'>Ninguno</option>";
  $option2="<option value='0'>Ninguno</option>";
  $option3="<option value='0'>Ninguno</option>";

  while($row = mysqli_fetch_array($result)){
    $option = $option."<option value='$row[0]' data-price='$row[2]'>$row[1]</option>";
  }

  $sql="SELECT * FROM telephone";
  $result = mysqli_query($conn,$sql) or die( "Error en query:".mysql_error() );

  while($row = mysqli_fetch_array($result)){
    $option2 = $option2."<option value='$row[0]' data-price='$row[5]'>$row[1]</option>";
  }

  $sql="SELECT * FROM internet";
  $result = mysqli_query($conn,$sql) or die( "Error en query:".mysql_error() );

  while($row = mysqli_fetch_array($result)){
    $option3 = $option3."<option value='$row[0]' data-price='$row[3]'>$row[1]</option>";
  }
*/
include_once 'includes/obtener-internet.php';
include_once 'includes/obtener-telefono.php';
include_once 'includes/obtener-tv.php';
$option="";
$option2="";
$option3="";
for ($i=0; $i < count($tv) ; $i++)
  $option=$option."<option value='{$tv[$i]['cable_id']}'>{$tv[$i]['plan_name']}</option>";

for ($i=0; $i < count($telephone) ; $i++)
  $option2=$option2."<option value='{$telephone[$i]['phone_id']}'>{$telephone[$i]['plan_name']}</option>";;


for ($i=0; $i < count($internet) ; $i++)
  $option3=$option3."<option value='{$internet[$i]['internet_id']}'>{$internet[$i]['plan_name']}</option>";

 ?>



<!DOCTYPE html>
 <html class="no-js">
    <!-- Header -->
        <?php
            require('views/admin-header.php');
        ?>
    <!-- Header -->

    <body>
    <!-- Admin Side Bar -->
        <?php
            require('views/side-bar.php');
        ?>
    <!-- Admin Side Bar -->

    <!-- Right Panel -->
        <div id="right-panel" class="right-panel">

    <!-- Header-->
        <?php
            require('views/module-header.php');
        ?>
    <!-- Header-->

    <div class="content mt-3">
            <div class="content">
            <h1 class="pb-3">Nuevo Paquete</h1>
                <form action="includes/package.php" method="POST">
                  <div class="form-group row">
                      <label  class="col-sm-2 col-form-label">Nombre del plan</label>
                      <div class="col-sm-10 col-md-2">
                      <input type="text" name="plan_name" class="form-control" id="plan_name" required>
                      </div>
                  </div>
                  <div class="form-group row">
                    <label  class="col-sm-2 col-form-label">Selecciona los planes</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <select name="cable" id="cable" class="form-control">
                      <option selected disabled>Servicio de TV</option>
                      <option value='0'>Ninguno</option>
                      <?php echo $option ?>
                    </select>
                    &nbsp;&nbsp;
                    <select name="phone" id="phone" class="form-control" >
                      <option selected disabled>Servicio de Telefonia</option>
                      <option value='0'>Ninguno</option>
                      <?php echo $option2 ?>
                    </select>
                    &nbsp;&nbsp;

                    <select name="internet" id="internet" class="form-control">
                      <option selected disabled>Servicio de Internet</option>
                      <option value='0'>Ninguno</option>
                      <?php echo $option3 ?>
                    </select>
                  </div>
                  <div class="form-group row">
                      <label  class="col-sm-2 col-form-label">Precio</label>
                      <div class="col-sm-10 col-md-2">
                      <input type="text" disabled name="price" class="form-control" id="price">
                      </div>
                  </div>
                  <button type="submit" name="submit" class="btn btn-primary center">Crear</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Right Panel -->



    <!-- Scripts -->
        <?php
            require('views/admin-footer.php');
        ?>

        <!--<script>
          var select = document.getElementById('cable');
          select.addEventListener('change',
          function(){
            var selectedOption = this.options[select.selectedIndex];
            console.log(selectedOption.value + ': ' + selectedOption.text);
          });

        </script>-->

    <!-- Scripts -->

</html>
