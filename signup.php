<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="">
    <!-- Head -->
        <?php
            require('views/head.php');
        ?>
    <!-- /Head -->
    <body>
    <!-- header-->
        <?php
            include('views/header.php');
        ?>
    <!--/header -->

    <!-- Login -->
        <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4"></div>
                    <div class="col-sm-12 col-md-4 pt-6">
                    <form action="includes/signup.inc.php" method="POST">
                        <div class="form-group">
                            <label for="exampleInputName">Nombre</label>
                            <input type="text" name="first" class="form-control" id="exampleInputName" aria-describedby="nameHelp" placeholder="Nombre" require >
                        </div>
                        <div class="form-group">
                            <label for="exampleInputLastName">Apellido</label>
                            <input type="text" name="last" class="form-control" id="exampleInputLastName" aria-describedby="lastNameHelp" placeholder="Apellido" require>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail">Correo Electrónico</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Correo electrónico" require>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail">Nombre de usuario</label>
                            <input type="text" name="uid" class="form-control" id="exampleInputUid" aria-describedby="uidHelp" placeholder="Nombre de usuario" require>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña</label>
                            <input type="password" name="pwd" class="form-control" id="exampleInputPassword1" placeholder="Password" require>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Sign up</button>
                    </form>
                    </div>
                    <div class="col-sm-12 col-md-4"></div>
                </div>
        </div>
    <!-- Login -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>
