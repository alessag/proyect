<!DOCTYPE html>
 <html class="no-js"> 
    <!-- Header -->
        <?php
            require('views/admin-header.php');
        ?>
    <!-- Header -->
    
    <body>
    <!-- Admin Side Bar -->
        <?php
            require('views/side-bar.php');
        ?>
    <!-- Admin Side Bar -->

    <!-- Right Panel -->
        <div id="right-panel" class="right-panel">

    <!-- Header-->
        <?php
            require('views/module-header.php');
        ?>
    <!-- Header-->

    <div class="content mt-3">
            <div class="content">
                <h1 class="pb-3">Crear programa</h1>
                <form action="includes/program.php" method="POST">
                    <div class="form-group row">
                        <label for="channel_name" class="col-sm-2 col-form-label">Nombre del canal</label>
                        <?php
                            include_once 'includes/dbh.inc.php';
                            $sql="SELECT * FROM channel";
                            $result = mysqli_query($conn,$sql) or die( "Error en query:".mysql_error() );
                        ?>
                        <select name="channel_id">
                        <?php
                            while($row = mysqli_fetch_array($result)){
                        ?>
                        <option value="<?php echo $row['channel_id'];?>"><?php echo $row['channel_name'];?></option>
                        <?php
                            }
                        ?>
                        </select>
                      
                    </div>
                    <div class="form-group row">
                        <label for="program_name" class="col-sm-2 col-form-label">Nombre del programa</label>
                        <div class="col-sm-10 col-md-3">
                        <input type="text" name=program_name class="form-control" id="program_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="time_start" class="col-sm-2 col-form-label">Hora de inicio</label>
                        <div class="col-sm-10 col-md-3">
                        <input type="time" name=time_start class="form-control" id="time_start">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="time_finish" class="col-sm-2 col-form-label">Hora de fin</label>
                        <div class="col-sm-10 col-md-3">
                        <input type="time" name=time_finish class="form-control" id="time_finish">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date" class="col-sm-2 col-form-label">Fecha</label>
                        <div class="col-sm-10 col-md-3">
                        <input type="date" name=date class="form-control" id="date">
                        </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary center">Crear</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Right Panel -->
    
    <!-- Scripts -->
        <?php
            require('views/admin-footer.php');
        ?>
    <!-- Scripts -->

</html>