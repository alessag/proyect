<?php
    session_start();
?>
<!doctype html>
<html lang="en">
    <!-- Head -->
        <?php
            require('views/head.php');
        ?>
    <!-- /Head -->
  <body>
    <!-- header-->
        <?php
            include('views/header.php');
        ?>
    <!--/header -->
   
    <!-- Barra de navegación-->
        <div class="container-fluid" id="menu-container">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Servicios</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Planes</a>
                        <a class="dropdown-item" href="#">Programación</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Planes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Programación</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Ayuda</a>
                </li>
            </ul>
        </div>
    <!--/Barra de navegación -->

    <!-- Info home -->
        <div class="container-fluid" id="info-home">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6 pt-5">
                        <h2>Con nosotros podrás tener al alcance de tu mano servicios de</h2>
                        <h2>telefonia movil, cable e internet</h2>
                        <h2>Todo con un solo click.</h2>
                    </div>
                    <div class="col-sm-12 col-md-6 pl-0 pr-0">
                        <img class="img-fluid" src="images/info-home-img.jpg" alt="">
                    </div>
                </div>
               
            </div>
            
        </div>
    <!-- /Info home -->

    <!-- Nuestros servicios -->
        <div class="container-fluid" id="our-services">
            <div class="row justify-content-center align-items-center">
                <div class="col phone-img">
                </div>
                <div class="col tv-img">
                </div>
                <div class="col internet-img">
                </div>
            </div>
        </div>
    <!-- /Nuestros servicios -->

    <!-- Footer -->
        <?php
            include('views/footer.php');
        ?>
    <!-- /Footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>