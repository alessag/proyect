<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="">
    <!-- Head -->
        <?php
            require('views/head.php');
        ?>
    <!-- /Head -->
    <body>
     <!-- header-->
        <?php
            include('views/header.php');
        ?>
    <!--/header -->

    <!-- Login -->
        <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4"></div>
                    <div class="col-sm-12 col-md-4 pt-6">
                    <form action="includes/login.inc.php" method="POST">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Correo electronico o nombre de usuario</label>
                            <input type="text" name="uid" class="form-control"  placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Contraseña</label>
                            <input type="password" name="pwd" class="form-control"  placeholder="Password">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <a  href="signup.php">Registrate</a>
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Login</button>
                        
                    </form>
                    </div>
                    <div class="col-sm-12 col-md-4"></div>
                </div>
        </div>
    <!-- Login -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>
