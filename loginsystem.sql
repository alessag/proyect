-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-10-2018 a las 19:24:47
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `loginsystem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cable`
--

CREATE TABLE `cable` (
  `cable_id` int(11) NOT NULL,
  `plan_name` varchar(256) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cable`
--

INSERT INTO `cable` (`cable_id`, `plan_name`, `price`) VALUES
(1, 'Basico', 1231),
(2, 'Moderado', 2000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `channel`
--

CREATE TABLE `channel` (
  `channel_id` int(11) NOT NULL,
  `channel_name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `channel`
--

INSERT INTO `channel` (`channel_id`, `channel_name`) VALUES
(1, 'asdas'),
(2, 'dsadas'),
(3, 'Natgeo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internet`
--

CREATE TABLE `internet` (
  `internet_id` int(11) NOT NULL,
  `plan_name` varchar(50) NOT NULL,
  `mb` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `internet`
--

INSERT INTO `internet` (`internet_id`, `plan_name`, `mb`, `price`) VALUES
(1, 'Nutella', 2, 500),
(2, 'Hellooooooo', 5, 20),
(3, 'Hellooooooo', 5, 20),
(4, 'Hellooooooo', 5, 20),
(5, 'Galack', 3, 100),
(6, 'Chonut', 3, 0),
(7, 'Pirulin', 5, 0),
(8, 'Pirulin', 5, 230),
(9, 'Galack', 0, 100),
(10, 'Chonut', 3, 100),
(11, 'Nutella', 2, 500),
(12, 'Nutella', 2, 500),
(13, 'Nutella', 45, 8521),
(14, 'Chonut', 2, 44),
(15, '', 2, 500),
(16, 'Pirulin', 0, 100),
(17, 'Chonut', 66, 88),
(18, 'Galack', 444, 65),
(19, 'Nutella', 2, 500),
(20, 'CriCri', 5, 650),
(21, 'Chonut', 20, 100),
(22, 'Prueba', 100, 203);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `program`
--

CREATE TABLE `program` (
  `program_id` int(11) NOT NULL,
  `program_name` varchar(256) NOT NULL,
  `time_start` time NOT NULL,
  `time_finish` time NOT NULL,
  `date_p` date NOT NULL,
  `channel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `program`
--

INSERT INTO `program` (`program_id`, `program_name`, `time_start`, `time_finish`, `date_p`, `channel_id`) VALUES
(1, 'Genius', '00:21:00', '13:21:00', '2019-11-24', 3),
(2, 'x', '14:22:00', '15:21:00', '2018-10-24', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telephone`
--

CREATE TABLE `telephone` (
  `phone_id` int(11) NOT NULL,
  `plan_name` varchar(50) NOT NULL,
  `mb` int(11) NOT NULL,
  `sms` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telephone`
--

INSERT INTO `telephone` (`phone_id`, `plan_name`, `mb`, `sms`, `min`, `price`) VALUES
(1, 'Nutella', 2, 100, 500, 100),
(2, 'Nutella', 2, 100, 500, 100),
(3, 'Nutella', 2, 100, 500, 500),
(4, 'Nutella', 2, 100, 500, 100),
(5, 'Nutella', 2, 100, 500, 100),
(6, 'Nutella', 2, 100, 500, 5001),
(7, 'Nutella', 2, 100, 500, 500),
(8, 'Nutella', 2, 100, 500, 500),
(9, 'Nutella', 3, 100, 500, 150);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_first` varchar(256) NOT NULL,
  `user_last` varchar(256) NOT NULL,
  `user_email` varchar(256) NOT NULL,
  `user_uid` varchar(256) NOT NULL,
  `user_pwd` varchar(256) NOT NULL,
  `user_registry_date` date NOT NULL,
  `state` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `user_first`, `user_last`, `user_email`, `user_uid`, `user_pwd`, `user_registry_date`, `state`) VALUES
(2, 'Tiziana', 'Amicarella Girardi', 'tiziana@gmail.com', 'Tami.gi', '$2y$10$8wtvvOI2xmf1bUcy3pWSWeAqJHIWbVKBdp11DFsh/Llw.darAWhtW', '2018-09-23', 0),
(3, 'Maria', 'Perez', 'maria@gmail.com', 'maria', '$2y$10$D4MefBIHxSu7oK9qjG5KEOWQuiiC6Nlux7zNObH0AxrmnO1hIQ0PS', '2018-09-08', 0),
(4, 'Alessa', 'Amicarella G', 'alessamica@gmail.com', 'Aless', '$2y$10$wBYlIDDzKQqDELet0NBhAO6BeQa.00DXmXzAQCT3r0RRCagedddQy', '2018-10-03', 0),
(5, 'Alessandra', 'Amicarella Girardi', 'alessamica@gmail.com', 'Aless26', '$2y$10$5NCphhehWCVcIdPXL47LN.txE6YyIR.hdt57MeziVZOB3W97OjrM6', '0000-00-00', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cable`
--
ALTER TABLE `cable`
  ADD PRIMARY KEY (`cable_id`);

--
-- Indices de la tabla `channel`
--
ALTER TABLE `channel`
  ADD PRIMARY KEY (`channel_id`);

--
-- Indices de la tabla `internet`
--
ALTER TABLE `internet`
  ADD PRIMARY KEY (`internet_id`);

--
-- Indices de la tabla `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`program_id`),
  ADD KEY `channel_id` (`channel_id`);

--
-- Indices de la tabla `telephone`
--
ALTER TABLE `telephone`
  ADD PRIMARY KEY (`phone_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cable`
--
ALTER TABLE `cable`
  MODIFY `cable_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `channel`
--
ALTER TABLE `channel`
  MODIFY `channel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `internet`
--
ALTER TABLE `internet`
  MODIFY `internet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `program`
--
ALTER TABLE `program`
  MODIFY `program_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `telephone`
--
ALTER TABLE `telephone`
  MODIFY `phone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
