
<?php
    include_once('includes/obtener-paquetes.php');
?>

<!DOCTYPE html>
 <html class="no-js">

    <!-- Header -->
        <?php
            require('views/admin-header.php');
        ?>

    <!-- Header -->

    <body>
    <!-- Admin Side Bar -->
        <?php
            require('views/side-bar.php');
        ?>
    <!-- Admin Side Bar -->

    <!-- Right Panel -->
        <div id="right-panel" class="right-panel">

    <!-- Header-->
        <?php
            require('views/module-header.php');
        ?>
    <!-- Header-->

    <div class="content mt-3">
            <div class="content">
            <h1 class="pb-3">Paquetes Disponibles</h1>
            <div class="card-columns">
            <?php
              for ($i=0; $i <count($package) ; $i++) {
                $price=0; ?>
                <div class="card border-success mb-3" style="max-width: 18rem;">
                  <h5 class="card-header bg-transparent border-success">Paquete: <?php echo $package[$i][0]; ?> </h5>
                    <div class="card-body text-success">
                      <h6 class="card-title">Disfruta de los planes: </h6>
                        <ul class="list-group list-group-flush">
                          <?php for ($j=1; $j < 4; $j++) {
                            if($package[$i][$j]!=NULL)
                            echo'<li class="list-group-item">'.$package[$i][$j].'</li>';
                          }?>
                        </ul>
                        <?php for($k=4; $k<7 ;$k++){
                          if($package[$i][$k]!=NULL)
                            $price=$price+$package[$i][$k];
                        }?>
                        <div class="card-footer bg-transparent border-success">Precio del paquete: <?php echo $price; ?> </div>
                    </div>
                </div>
            <?php
              }
            ?>
            <!--<div class="card bg-light mb-3" style="max-width: 18rem;">
              <div class="card-header">Header</div>
                <div class="card-body">
                  <h5 class="card-title">Light card title</h5>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>-->
            </div>
            </div>
        </div>
    </div>
    <!-- Right Panel -->

    <!-- Scripts -->
        <?php
            require('views/admin-footer.php');
        ?>
    <!-- Scripts -->

</html>
