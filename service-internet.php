<!DOCTYPE html>
 <html class="no-js"> 
    <!-- Header -->
        <?php
            require('views/admin-header.php');
        ?>
    <!-- Header -->
    
    <body>
 <!-- Admin Side Bar -->
 <?php
            require('views/side-bar.php');
        ?>
    <!-- Admin Side Bar -->

    <!-- Right Panel -->
        <div id="right-panel" class="right-panel">

    <!-- Header-->
        <?php
            require('views/module-header.php');
        ?>
    <!-- Header-->

    <div class="content mt-3">
            <div class="content">
            <h1 class="pb-3">Servicio de Internet</h1>
                <form action="includes/internet.php" method="POST">
                    <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Nombre del plan</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="plan_name" class="form-control" id="plan_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">MB</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="mb" class="form-control" id="mb">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Precio</label>
                        <div class="col-sm-10 col-md-2">
                        <input type="text" name="price" class="form-control" id="price">
                        </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary center">Crear</button>
                </form>
            </div>
        </div>
    </div>
    <!-- Right Panel -->
    
    <!-- Scripts -->
        <?php
            require('views/admin-footer.php');
        ?>
    <!-- Scripts -->

</html>